package models

import (
	"gorm.io/gorm"
	"time"
)

type User struct {
	gorm.Model

	ID        uint      `gorm:"primary_key";autoIncrement`
	Name      string    `gorm:"unique;type:varchar(255);not null"`
	Email     string    `gorm:"unique;type:varchar(255);unique_index;not null"`
	Password  string    `gorm:"type:varchar(255);not null"`
	CreatedAt time.Time `gorm:"not null"`
	UpdatedAt time.Time `gorm:"not null"`
}
