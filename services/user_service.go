package services

import (
	"du/models"
	"du/repositories"
)

// UserService 用户服务
type UserService struct {
	userRepository *repositories.UserRepository
}

// NewUserService 创建用户服务
func NewUserService(userRepository *repositories.UserRepository) *UserService {
	return &UserService{
		userRepository: userRepository,
	}
}

// GetUserByID 根据ID获取用户
func (s *UserService) GetUserByID(id uint) (*models.User, error) {
	return s.userRepository.GetUserByID(id)
}

// CreateUser 创建用户
func (s *UserService) CreateUser(user *models.User) error {
	return s.userRepository.CreateUser(user)
}

// GetUser 创建用户
func (s *UserService) GetUser() ([]models.User, error) {
	return s.userRepository.GetUser()
}
