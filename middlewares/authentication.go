package middlewares

import (
	"github.com/gin-gonic/gin"
)

func Authentication() gin.HandlerFunc {
	return func(c *gin.Context) {
		// 在这里进行认证处理
		// ...

		c.Next()
	}
}