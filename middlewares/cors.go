package middlewares

import "github.com/gin-gonic/gin"

func Cors() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		// 执行对应的逻辑
		ctx.Next()
	}
}
