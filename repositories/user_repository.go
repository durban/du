package repositories

import (
	"du/database"
	"du/models"
)

// UserRepository 用户仓库
type UserRepository struct {
	db *database.Database
}

func NewUserRepository() *UserRepository {
	db := database.GetDB()

	return &UserRepository{db: db}
}

// GetUserByID 根据ID获取用户
func (r *UserRepository) GetUserByID(id uint) (*models.User, error) {
	var user models.User
	result := r.db.GetSlaveDB().First(&user, id)
	if result.Error != nil {
		return nil, result.Error
	}

	return &user, nil
}

// CreateUser 创建用户
func (r *UserRepository) CreateUser(user *models.User) error {
	result := r.db.GetMasterDB().Create(user)
	return result.Error
}

// GetUser 获取所有用户
func (r *UserRepository) GetUser() ([]models.User, error) {
	var user []models.User

	result := r.db.GetSlaveDB().Find(&user)

	if result.Error != nil {
		return user, result.Error
	}

	return user, nil
}
