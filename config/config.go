package config

import (
	"fmt"
	"github.com/spf13/viper"
)

type Config struct {
	App     AppConfig `mapstructure:"app"`
	Mysql   DBConfigs `mapstructure:"mysql"`
	Mongodb DBConfig  `mapstructure:"mongodb"`
	Redis   DBConfig  `mapstructure:"redis"`
}

type AppConfig struct {
	Port string `mapstructure:"port"`
	Mode string `mapstructure:"mode"`
}

type DBConfigs struct {
	Master DBConfig   `mapstructure:"master"`
	Slave  []DBConfig `mapstructure:"slaves"`
}

type DBConfig struct {
	Host     string `mapstructure:"host"`
	Port     string `mapstructure:"port"`
	Username string `mapstructure:"username"`
	Password string `mapstructure:"password"`
	Database string `mapstructure:"database"`
}

var App Config

func Load() {
	v := viper.New()

	v.SetConfigType("yaml")
	v.AddConfigPath("./config")

	v.SetConfigName("app")
	// 加载app第一个配置文件
	err := v.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("无法读取配置文件: %s", err))
	}

	// 加载database第二个配置文件
	v.SetConfigName("database")
	err = v.MergeInConfig()
	if err != nil {
		panic(fmt.Errorf("无法加载附加配置文件: %s", err))
	}

	if err := v.Unmarshal(&App); err != nil {
		panic(err)
	}

	//fmt.Print(config)
}
