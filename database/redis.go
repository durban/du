package database

import (
	"du/config"

	"github.com/go-redis/redis/v8"
)

var RedisClient *redis.Client

func InitRedisDB() (*redis.Client, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     config.App.Redis.Host,
		Password: config.App.Redis.Password,
		DB:       0,
	})

	RedisClient = client

	return client, nil
}