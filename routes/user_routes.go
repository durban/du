package routes

import (
	"du/controllers"
	"github.com/gin-gonic/gin"
)

// RegisterUserRoutes 注册用户相关的路由
func RegisterUserRoutes(engine *gin.Engine, userController *controllers.UserController) {
	engine.GET("/users/:id", userController.GetByID)
	engine.POST("/users", userController.CreateUser)
	engine.GET("/users", userController.GetUser)
}
