package routes

import (
	"du/controllers"
	"github.com/gin-gonic/gin"
)

// RegisterRoutes 注册所有路由
func RegisterRoutes(router *gin.Engine) {

	userController := controllers.NewUserController()

	RegisterUserRoutes(router, userController)
}
