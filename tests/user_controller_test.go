package tests

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"du/controllers"
	"du/services"
	"du/repositories"
)

func TestGetUserByID(t *testing.T) {
	gin.SetMode(gin.TestMode)

	// 创建虚拟的 HTTP 请求和响应
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)

	// 创建 UserRepository 和 UserService 的模拟实例
	userRepo := &repositories.UserRepository{
		DB: nil, // TODO: 添加测试数据库连接
	}
	userService := &services.UserService{
		Repo: userRepo,
	}

	// 创建 UserController 实例
	userController := &controllers.UserController{
		Service: userService,
	}

	// 设置请求上下文的参数
	c.Params = gin.Params{
		gin.Param{Key: "id", Value: "1"},
	}

	// 调用控制器方法
	userController.GetUserByID(c)

	// 验证状态码和响应结果
	assert.Equal(t, http.StatusOK, w.Code)
	assert.Contains(t, w.Body.String(), "user")
}